package org.helen;

import org.helen.model.Instrument;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Objects;

import static org.junit.Assert.*;

/**
 * Created by Helen on 14.01.2018.
 */
public class ApplicationTest {

    @org.junit.Test
    public void processFile() throws Exception {
        String filename = "src/main/resources/sample.csv";
        Application application = new Application();
        Hashtable<Instrument, Double> calculatedResult = application.processFile(filename);
        Hashtable<Instrument, Double> etalonResult = new Hashtable<>();
        etalonResult.put(new Instrument("Instrument1"), 61.69);
        etalonResult.put(new Instrument("Instrument2"), 0.01);
        etalonResult.put(new Instrument("Instrument3"), 700.0);
        etalonResult.put(new Instrument("Other"), 0.0);
        Assert.assertEquals(calculatedResult.keySet().size(), etalonResult.keySet().size());
        Object []keysCalculated = calculatedResult.keySet().stream().map(Instrument::getName).toArray();
        Object []keysEtalon = etalonResult.keySet().stream().map(Instrument::getName).toArray();
        Arrays.sort(keysCalculated);
        Arrays.sort(keysEtalon);
        Assert.assertArrayEquals(keysCalculated, keysEtalon);
        for (Instrument instrument : etalonResult.keySet()){
            Assert.assertEquals(calculatedResult.get(instrument), etalonResult.get(instrument), 1e-6);
        }
    }

}