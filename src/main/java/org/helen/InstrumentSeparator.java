package org.helen;

import org.helen.model.FinancialRecord;
import org.helen.model.Instrument;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Helen on 14.01.2018.
 */
public class InstrumentSeparator {

    private static InstrumentSeparator instance = null;

    private Hashtable<Instrument, List<FinancialRecord>> hashTable = new Hashtable<>();

    private InstrumentSeparator(){

    }

    private void clear(){
        for (Instrument instrument : hashTable.keySet()){
            hashTable.get(instrument).clear();
        }
    }

    public Hashtable<Instrument, List<FinancialRecord>> separateBatch(List<FinancialRecord> list){
        clear();
        for (FinancialRecord record : list){
            Instrument instrument = record.getInstrument();
            if (!hashTable.containsKey(instrument))
                hashTable.put(instrument, new ArrayList<>());
            hashTable.get(instrument).add(record);
        }
        return hashTable;
    }

    public static InstrumentSeparator getInstance(){
        if (instance == null)
            instance = new InstrumentSeparator();
        return instance;
    }

}
