package org.helen.model;

/**
 * Created by Helen on 13.01.2018.
 */
public class Instrument {

    private String name;

    public Instrument(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public int hashCode(){
        return name.toLowerCase().hashCode();
    }

    public boolean equals(Object instrument) {
        if (!(instrument instanceof Instrument))
            return false;
        return this == instrument || this.name.toLowerCase().compareTo(((Instrument) instrument).getName().toLowerCase()) == 0;
    }

}
