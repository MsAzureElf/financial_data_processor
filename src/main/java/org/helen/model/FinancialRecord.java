package org.helen.model;

import java.time.LocalDate;

/**
 * Created by Helen on 13.01.2018.
 */
public class FinancialRecord {

    private Instrument instrument;

    private LocalDate date;

    private double price;

    public FinancialRecord(Instrument instrument, LocalDate date, Double price){
        this.instrument = instrument;
        this.date = date;
        this.price = price;
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getPrice() {
        return price;
    }
}
