package org.helen.configuration;

import java.util.List;

/**
 * Created by Helen on 14.01.2018.
 */
public class MappingConfiguration {

    private List<MappingRecord> instruments;

    public List<MappingRecord> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<MappingRecord> instruments) {
        this.instruments = instruments;
    }
}
