package org.helen.configuration;

/**
 * Created by Helen on 14.01.2018.
 */
public class MappingRecord {

    private String instrumentName;
    private String handlerName;

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }


}
