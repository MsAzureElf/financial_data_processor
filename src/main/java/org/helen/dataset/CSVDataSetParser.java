package org.helen.dataset;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.helen.exceptions.RecordFormatException;
import org.helen.model.FinancialRecord;
import org.helen.model.Instrument;

/**
 * Created by Helen on 14.01.2018.
 */
public class CSVDataSetParser {

    Iterator<CSVRecord> iterator;

    private CSVFormat csvFormat = CSVFormat.newFormat(',');

    private static final int BATCH_SIZE = 100000;

    public CSVDataSetParser(String filename) throws IOException {
        Reader reader = new FileReader(filename);
        CSVParser csvParser = csvFormat.parse(reader);
        iterator = csvParser.iterator();
    }

    public boolean hasMoreRecords(){
        return iterator != null && iterator.hasNext();
    }

    public List<FinancialRecord> getNextBatch() throws RecordFormatException {
        int counter = 0;
        List<FinancialRecord> list = new ArrayList<>();
        for (;counter<BATCH_SIZE&&iterator.hasNext();++counter){
            CSVRecord csvRecord = iterator.next();
            String instrument;
            LocalDate date;
            double price;
            try {
                instrument = csvRecord.get(0);
                date = LocalDate.parse(csvRecord.get(1), DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.forLanguageTag("en")));
                price = Double.parseDouble(csvRecord.get(2));
            } catch (Exception e){
                throw new RecordFormatException(counter);
            }
            FinancialRecord finRecord = new FinancialRecord(new Instrument(instrument), date, price);
            list.add(finRecord);
        }
        return list;
    }

}
