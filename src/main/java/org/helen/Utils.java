package org.helen;

import org.helen.model.FinancialRecord;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Helen on 14.01.2018.
 */
public class Utils {

    public static double getSum(List<FinancialRecord> list){
        return list.stream().map(FinancialRecord::getPrice)
                .collect(Collectors.summingDouble(i->i));
    }

}
