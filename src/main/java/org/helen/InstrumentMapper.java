package org.helen;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.helen.configuration.MappingConfiguration;
import org.helen.configuration.MappingRecord;
import org.helen.model.Instrument;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

/**
 * Created by Helen on 14.01.2018.
 */
public class InstrumentMapper {

    private static String MAPPING_FILE = "src/main/resources/instrument_mapping.yml";
    private static String ALGORITHM_PREFIX = "org.helen.algorithms.";

    private static Hashtable<Instrument, CalculationAlgorithm> instrumentMapping = null;

    public static Hashtable<Instrument, CalculationAlgorithm> getInstrumentMapping() throws IOException {
        if (instrumentMapping != null)
            return instrumentMapping;
        instrumentMapping = new Hashtable<>();
        ObjectMapper objMapping = new ObjectMapper(new YAMLFactory());
        MappingConfiguration configuration = objMapping.readValue(new File(MAPPING_FILE), MappingConfiguration.class);
        for (MappingRecord mappingRecord : configuration.getInstruments()){
            try {
                Class className = Class.forName(ALGORITHM_PREFIX + mappingRecord.getHandlerName());
                CalculationAlgorithm algorithm = (CalculationAlgorithm)className.newInstance();
                instrumentMapping.put(new Instrument(mappingRecord.getInstrumentName()), algorithm);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return instrumentMapping;
    }

}