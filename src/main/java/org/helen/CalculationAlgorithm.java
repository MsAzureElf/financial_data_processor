package org.helen;

import org.helen.model.FinancialRecord;

import java.util.List;

/**
 * Created by Helen on 13.01.2018.
 */

public interface CalculationAlgorithm {

    double processNextBatch(List<FinancialRecord> recordList);

}
