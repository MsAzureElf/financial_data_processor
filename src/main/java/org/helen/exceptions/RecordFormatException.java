package org.helen.exceptions;

import java.io.IOException;

/**
 * Created by Helen on 14.01.2018.
 */
public class RecordFormatException extends IOException {

    public RecordFormatException(){
        super();
    }

    public RecordFormatException(int exceptionLineNumber){
        super(String.format("Line %d is wrongly formatted!", exceptionLineNumber));
    }




}
