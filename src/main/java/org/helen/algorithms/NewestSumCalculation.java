package org.helen.algorithms;

import org.helen.CalculationAlgorithm;
import org.helen.model.FinancialRecord;

import java.time.LocalDate;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by Helen on 14.01.2018.
 */
public class NewestSumCalculation implements CalculationAlgorithm {

    private static class DatedPrice implements Comparable<DatedPrice> {

        private LocalDate date;
        private double price;

        public DatedPrice(LocalDate date, double price){
            this.date = date;
            this.price = price;
        }

        @Override
        public int compareTo(DatedPrice o) {
            return this.date.compareTo(o.date);
        }

        public double getPrice() {
            return price;
        }

        public LocalDate getDate() {
            return date;
        }

    }

    private PriorityQueue<DatedPrice> newestPrices = new PriorityQueue<>();

    public double processNextBatch(List<FinancialRecord> recordList) {
        for (FinancialRecord financialRecord : recordList){
            newestPrices.add(new DatedPrice(financialRecord.getDate(), financialRecord.getPrice()));
            if (newestPrices.size() > 10)
                newestPrices.poll();
        }
        double sum = 0;
        for (DatedPrice newestPrice : newestPrices) sum += newestPrice.getPrice();
        return sum;
    }

}
