package org.helen.algorithms;

import org.helen.CalculationAlgorithm;
import org.helen.model.FinancialRecord;

import java.util.List;

/**
 * Created by Helen on 14.01.2018.
 */
public class MaxCalculation implements CalculationAlgorithm {

    private double currentMax = Double.MIN_VALUE;

    public double processNextBatch(List<FinancialRecord> recordList) {
        for (FinancialRecord financialRecord : recordList){
            currentMax = Math.max(currentMax, financialRecord.getPrice());
        }
        return currentMax;
    }

}
