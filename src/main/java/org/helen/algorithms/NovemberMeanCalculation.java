package org.helen.algorithms;

import org.helen.CalculationAlgorithm;
import org.helen.Utils;
import org.helen.model.FinancialRecord;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Helen on 13.01.2018.
 */
public class NovemberMeanCalculation implements CalculationAlgorithm {

    public static int MONTH = Calendar.NOVEMBER + 1;
    public static int YEAR = 2014;

    private double currentSum = 0;

    public double processNextBatch(List<FinancialRecord> recordList) {
        List<FinancialRecord> filteredRecords = recordList.stream()
                .filter(record -> (record.getDate().getMonthValue() == MONTH && record.getDate().getYear() == YEAR))
                .collect(Collectors.toList());
        currentSum += Utils.getSum(filteredRecords);
        return currentSum;
    }

}
