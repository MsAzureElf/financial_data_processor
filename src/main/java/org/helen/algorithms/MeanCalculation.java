package org.helen.algorithms;

import org.helen.CalculationAlgorithm;
import org.helen.Utils;
import org.helen.model.FinancialRecord;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Helen on 13.01.2018.
 */
public class MeanCalculation implements CalculationAlgorithm {

    private long currentCount = 0;
    private double currentSum = 0;

    public double processNextBatch(List<FinancialRecord> recordList) {
        currentCount += recordList.size();
        currentSum += Utils.getSum(recordList);
        if (currentCount > 0)
            return currentSum / currentCount;
        return 0;
    }

}
