package org.helen;

import org.helen.dataset.CSVDataSetParser;
import org.helen.exceptions.RecordFormatException;
import org.helen.model.FinancialRecord;
import org.helen.model.Instrument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Helen on 14.01.2018.
 */
public class Application {

    private static String PROCESSED_FILENAME = "src/main/resources/sample.csv";

    public Hashtable<Instrument, Double> processFile(String filename){
        Hashtable<Instrument, CalculationAlgorithm> instrumentConfiguration = null;
        CSVDataSetParser dataSetParser = null;
        try {
            instrumentConfiguration = InstrumentMapper.getInstrumentMapping();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        try {
            dataSetParser = new CSVDataSetParser(filename);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        List<Instrument> availableInstruments = instrumentConfiguration.keySet().stream().collect(Collectors.toList());
        InstrumentSeparator separator = InstrumentSeparator.getInstance();
        Hashtable<Instrument, Double> currentResult = new Hashtable<>();
        while (dataSetParser.hasMoreRecords()){
            List<FinancialRecord> list = null;
            try {
                list = dataSetParser.getNextBatch();
            } catch (RecordFormatException e) {
                System.out.println("Ignoring current batch due to critical error");
                e.printStackTrace();
            }
            Hashtable<Instrument, List<FinancialRecord>> separatedInstruments = separator.separateBatch(list);
            for (Instrument instrument : availableInstruments){
                List<FinancialRecord> currentBatch = null;
                if (separatedInstruments.containsKey(instrument))
                    currentBatch = separatedInstruments.get(instrument);
                else
                    currentBatch = new ArrayList<>();
                double instrumentResult = instrumentConfiguration.get(instrument).processNextBatch(currentBatch);
                System.out.println(String.format("Instrument: %s, result: %.6f", instrument, instrumentResult));
                currentResult.put(instrument, instrumentResult);
            }
        }
        return currentResult;
    }

    public static void main(String[] args) {
        Application application = new Application();
        Hashtable<Instrument, Double> results = application.processFile(PROCESSED_FILENAME);
        System.out.println("Final calculation results:");
        for (Instrument instrument : results.keySet()){
            System.out.println(String.format("Instrument: %s, result: %.6f", instrument, results.get(instrument)));
        }
    }

}
